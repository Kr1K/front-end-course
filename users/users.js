$(function() {

    buildTableData();

    $('#page-selection').bootpag({
        total: 1,
        page: 1,
        maxVisible: 5,
        leaps: true
    }).on('page', function (event, num) {
        buildTableData(num - 1);
    });

    $(document).on('click', '#logOut', function () {
        removeToken();
        location.href = '/';
    });

    $(document).on('keyup', '#userSearch', function () {
    	delay(buildTableData, 350);
    });

    $(document).on('blur', '#modalUser :input', function (event) {
        $(this).closest('.form-group').addClass('was-validated');
    });

    $(document).on('click', '.open_delete_user_modal', function () {

        let userId = $(this).closest('tr').attr('user_id');
        let firstName = $(this).closest('tr').find('td.firstName').text();

        $('#modalDelete .firstName').text(firstName);
        $('#modalDelete #delete_user').attr('user_id', userId).attr('firstName', firstName);

        $('#modalDelete').modal('show');

    });

    $(document).on('click', '.open_edit_user_modal', function () {

        $('#modalUser :input').val('');
        $('#modalUser .was-validated').removeClass('was-validated');

        $('#modalUser .modal-title').text($(this).data('title'));
        $('#modalUser .create').hide().find(':input').prop('disabled', true);
        $('#modalUser .edit').show();

        let userId = $(this).closest('tr').attr('user_id');

        $('#modalUser #edit_user').attr('user_id', userId);

        getUser(userId, handleSuccessGetUser);

        function handleSuccessGetUser(response) {
            $.each(response, function (key, value) {
                $('#modalUser [name=' + key + ']').val(value);
            });
        }

        $('#modalUser').modal('show');
    });


    $(document).on('click', '.open_create_user_modal', function () {

        $('#modalUser :input').val('');
        $('#modalUser .was-validated').removeClass('was-validated');

        $('#modalUser .modal-title').text($(this).data('title'))
        $('#modalUser .create').show().find(':input').prop('disabled', false);
        $('#modalUser .edit').hide();

        $('#modalUser').modal('show');
    });

    $(document).on('click', '#edit_user', function () {

        $('#userForm').addClass('was-validated');
        if (!$('#userForm')[0].checkValidity()) {
            return false;
        }

        let data = {
            firstName: $('#userForm [name=firstName]').val(),
            lastName: $('#userForm [name=lastName]').val()
        };

        editUser($(this).attr('user_id'), data, handleSuccessEditUser);

        function handleSuccessEditUser () {
            buildTableData(getCurrentPage());

            $('#modalUser').modal('hide');
            simpleAlert.show('success', 'User was edited');
        }

    });

    $(document).on('click', '#delete_user', function () {
        let firstName = $(this).attr('firstName');

        deleteUser($(this).attr('user_id'), handleSuccessDeleteUser);

        function handleSuccessDeleteUser () {
            buildTableData(getCurrentPage());

            $('#modalDelete').modal('hide');
            simpleAlert.show('info', `User <b>${firstName}</b> was deleted!!!`);
        }

    });

    $(document).on('click', '#create_user', function () {

        $('#userForm').addClass('was-validated');
        if (!$('#userForm')[0].checkValidity()) {
            return false;
        }

        let data = {
            email: $('#userForm [name=email]').val(),
            firstName: $('#userForm [name=firstName]').val(),
            lastName: $('#userForm [name=lastName]').val(),
            password: $('#userForm [name=password]').val()
        };

        createUser(data, handleSuccessCreateUser);

        function handleSuccessCreateUser () {
            buildTableData();

            $('#modalUser').modal('hide');
            simpleAlert.show('success', 'User was created!');
        }

    });

    $(document).on('click', 'table#users_table thead th[class^="sorting"]', function () {

        let that = this;

        $.each($('table#users_table thead th.sorting_asc, table#users_table thead th.sorting_desc'), function (i, element) {
            if (that !== element) {
                $(element)
                    .removeClass('sorting_asc')
                    .removeClass('sorting_desc')
                    .addClass('sorting');
            }
        });

        if ($(this).hasClass('sorting')) {
            $(this).removeClass('sorting').addClass('sorting_asc');
        } else if ($(this).hasClass('sorting_asc')) {
            $(this).removeClass('sorting_asc').addClass('sorting_desc');
        } else if ($(this).hasClass('sorting_desc')) {
            $(this).removeClass('sorting_desc').addClass('sorting');
        }

        buildTableData(getCurrentPage());
    });

    $(document).on('change', '#pageSize', function () {
        buildTableData();
    });

});

/**
 * Build body of table
 * @param page - page number
 */
function buildTableData (page = '0') {

    $('.processing').show();

    let request_data = {
        query: $('#userSearch').val(),
        page: parseInt(page),
        pageSize: parseInt($('#pageSize').val())
    };

    let sortByAndDirection = getSortByAndDirection($('table#users_table thead'));

    $.extend(request_data, sortByAndDirection);

    getUserList(request_data, handleSuccessGetUserList);

    function handleSuccessGetUserList (response) {

        if (response.pageable.offset >= response.totalElements && page > 0) {
            return buildTableData(page - 1);
        }

        $('.processing').hide();

        $('#page-selection').bootpag({
            total: response.totalPages,
            page: response.number + 1
        });

        drawPageInfo(response.size, response.number, response.totalElements);

        drawUsersTableBody(response.content, $('#users_table tbody'));

    }
}

/**
 * Get information for sorting data
 * @param $thead - parent jquery element
 * @return {{sortBy?: string, direction?: string}}
 */
function getSortByAndDirection ($thead) {

    let obj = {};

    let th = $('th.sorting_asc, th.sorting_desc', $thead)[0];

    if (typeof th !== 'undefined') {
        obj.sortBy = $(th).attr('name');
        if ($(th).hasClass('sorting_desc')) {
            obj.direction = 'desc';
        }
    }

    return obj;
}

/**
 * Get current page number
 * @return {number}
 */
function getCurrentPage () {
    return $('.pagination .page-item.active').data('lp') - 1;
}

/**
 * Build page info block
 * @param size - number elements on page
 * @param number - current page
 * @param totalElements - total elements
 */
function drawPageInfo(size, number, totalElements) {
    $('#page-info').html(`
        <p>Showing ${parseInt(size * number + 1)} to ${Math.min(parseInt(size * (number + 1)), totalElements)} of <b>${totalElements}</b> entries</p>
    `);
}

/**
 * Draws body of user table
 * @param users - user list
 * @param $tbody - parent jquery element
 */
function drawUsersTableBody (users, $tbody) {
    $tbody.html('');

    $.each(users, function (index, user) {

        let lastSignIn;
        if (user.lastSignIn && Array.isArray(user.lastSignIn)) {

            user.lastSignIn[1] -= 1;

            lastSignIn = new Date(Date.UTC(
                user.lastSignIn[0],
                user.lastSignIn[1],
                user.lastSignIn[2],
                user.lastSignIn[3],
                user.lastSignIn[4],
                user.lastSignIn[5]
            ));

            lastSignIn = moment(lastSignIn).format('YYYY-MM-DD HH:mm:ss');

        } else {
            lastSignIn = '';
        }

        $tbody.append(`
            <tr user_id="${user.id}">
                <td>${user.id}</td>
                <td class="firstName">${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${lastSignIn}</td>
                <td>
                    <button type="button" class="btn btn-outline-primary btn-sm mr-auto open_edit_user_modal" data-title="Edit user">
                        <i class="fa fa-edit fa-lg" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-outline-danger btn-sm open_delete_user_modal">
                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);
    });
}

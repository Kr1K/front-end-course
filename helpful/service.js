/**
 * Get user list
 * @param data {{ query?: string, page?: number, pageSize?: number, sortBy?: string, direction?: string }}
 * @param handleSuccess - callback function
 */
function getUserList (data, handleSuccess) {

    $.ajax({
        type: 'GET',
        url: 'https://report.inventorsoft.co/app/users',
        data: data,
        headers: {
            "Authorization": getToken()
        }
    }).then(
        function (response) {
            return handleSuccess(response);
        },
        function (error) {
            switchAjaxError(error.status, error);
        }
    );
}

/**
 * Get user info
 * @param userId - user's id
 * @param handleSuccess - callback function
 */
function getUser (userId, handleSuccess) {

    $.ajax({
        type: 'GET',
        url: 'https://report.inventorsoft.co/app/users/' + userId,
        headers: {
            "Authorization": getToken()
        }
    }).then(
        function (response) {
            return handleSuccess(response);
        },
        function (error) {
            switchAjaxError(error.status, error);
        }
    );

}

/**
 * Create user
 * @param userData {{email: string, firstName: string, lastName: string, password: string}}
 * @param handleSuccess - callback function
 */
function createUser (userData, handleSuccess) {

    $.ajax({
        type: 'POST',
        url: 'https://report.inventorsoft.co/app/users',
        data: JSON.stringify(userData),
        contentType: 'application/json',
        headers: {
            "Authorization": getToken()
        }
    }).then(
        function (response) {
            return handleSuccess();
        },
        function (error) {
            switchAjaxError(error.status, error);
        }
    );
}

/**
 *  Edit user
 * @param userId - user's id
 * @param userData {{firstName: string, lastName: string}}
 * @param handleSuccess - callback function
 */
function editUser (userId, userData, handleSuccess) {

    $.ajax({
        type: 'PATCH',
        url: 'https://report.inventorsoft.co/app/users/' + userId,
        headers: {
            "Authorization": getToken()
        },
        data: JSON.stringify(userData),
        contentType: 'application/json'
    }).then(
        function (response) {
            return handleSuccess();
        },
        function (error) {
            switchAjaxError(error.status, error);
        }
    );
}

/**
 * Delete user
 * @param userId - user's id
 * @param handleSuccess - callback function
 */
function deleteUser (userId, handleSuccess) {

    $.ajax({
        type: 'DELETE',
        url: 'https://report.inventorsoft.co/app/users/' + userId,
        headers: {
            "Authorization": getToken()
        },
        contentType: 'application/json'
    }).then(
        function (response) {
            return handleSuccess();
        },
        function (error) {
            switchAjaxError(error.status, error);
        }
    );
}
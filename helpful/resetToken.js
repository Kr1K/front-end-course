$(function() {

    window.setInterval(
        function () {

            $.ajax({
                type: 'GET',
                url: 'https://report.inventorsoft.co/app/refresh-token',
                headers: {
                    "Authorization": getToken()
                }

            }).then(
                function (response) {
                    setToken(response);
                },
                function (error) {
                    switchAjaxError(error.status, error);
                }

            );
        },
        2 * 60 * 1000
    );

});

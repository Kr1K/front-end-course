/**
 * Get token from local storage
 * @return {string | null}
 */
function getToken () {
    return localStorage.getItem('token');
}

/**
 * Set token to local storage
 * @param token
 * @return void
 */
function setToken (token) {
    localStorage.setItem('token', token);
}

/**
 * Remove token from local storage
 * @return void
 */
function removeToken () {
    localStorage.removeItem('token');
}

/**
 * Error handling
 * @param errorStatus - status code
 * @param error - raw response
 * @return void
 */
function switchAjaxError (errorStatus, error) {
    switch (errorStatus) {
        case 401:
            location.href = '/';
            break;
        default:
            simpleAlert.show('danger', 'something went wrong! Error code: "' + errorStatus + '"', false);

    }
}

/**
 * Executes callback function with delay.
 * If delay function call before expired delay time
 * previous call, when previous call will be canceled *
 */
var delay = (function () {
    let timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


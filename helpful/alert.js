var simpleAlert = {
    getCheckedType: function (type) {
        let types = [ 'primary', 'success', 'info', 'warning', 'danger', 'secondary', 'dark', 'light' ];

        let result = type;

        if (!$.inArray(type, types)) {
            result = 'info';
        }

        return result;
    },

    show: function (type, msg, close) {

        let $div = $('#message-container');

        if (typeof close === 'undefined') {
            close =  true;
        }

        type = this.getCheckedType(type);

        var $massege_block = $('<div>', {
            'class': 'show mb-3 alert alert-' + type,
            role: 'alert',
        }).append(
            '<button type="button" class="close ml-3" data-dismiss="alert" aria-label="Close">\
                <span aria-hidden="true">&times;</span>\
            </button>\
            <span class="msg-body">' + msg + '</span>'
        ).appendTo($div);

        $massege_block.fadeIn(1000).delay(2000);
        if (close) {
            $massege_block.fadeOut(5000);
        }

    }
};
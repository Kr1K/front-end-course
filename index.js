$(function() {

    $(document).on('blur', '.card-body :input', function (event) {
        $(this).closest('.form-group').addClass('was-validated')
    });

    $(document).on('click', '#login', function () {

        $('.card-body form').addClass('was-validated');
        if (!$('.card-body form')[0].checkValidity()) {
            simpleAlert.show('warning', 'Please fill in the required fields!');
            return false;
        }

        let data = JSON.stringify({
            userName: $('#userName').val(),
            password: $('#password').val()
        });

        $.ajax({
            type: 'POST',
            url: 'https://report.inventorsoft.co/app/no-auth/login',

            data: data,
            contentType: 'application/json'

        }).then(
            function (response) {
                setToken(response);
                location.href = 'users';
            },
            function (error) {
                switchAjaxError(error.status, error);
            }
        );

    });

    $(document).on('keypress', 'body', function (event) {
        if (event.which === 13) {
            $('#login').click();
        }
    });

});
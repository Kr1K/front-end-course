$(function() {

    $(document).on('click', '#reset', function() {
        $(this).addClass('disabled');

        let data = JSON.stringify({
            token: $('#token').val(),
            password: $('#password').val()
        });

        $.ajax({
            type: 'POST',
            url: 'https://report.inventorsoft.co/app/no-auth/forgetPassword/resetPassword',

            data: data,
            contentType: 'application/json'
        }).then(
            function (response) {
                location.href = '/';
            },
            function (error) {
                switchAjaxError(error.status, error);
            }
        );

    });

    $(document).on('keypress', 'body', function(event) {
        if (event.which === 13) {
            $("#reset").click();
        }
    });

});
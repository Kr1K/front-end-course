$(function() {

    $(document).on('blur', '.card-body :input', function (event) {
        $(this).closest('.form-group').addClass('was-validated')
    });

    $(document).on('click', 'button', function() {

        $('.card-body form').addClass('was-validated');
        if (!$('.card-body form')[0].checkValidity()) {
            simpleAlert.show('warning', 'Please fill in the required fields!');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: 'https://report.inventorsoft.co/app/no-auth/forgetPassword?email=' + $('#email').val(),

            contentType: 'application/json'
        }).then(
            function (response) {
                location.href = '../reset';
            },
            function (error) {
                switchAjaxError(error.status, error);
            }
        );

    });

    $(document).on('keypress', 'body', function(event) {
        if (event.which === 13) {
            $("button#login").click();
        }
    });

});